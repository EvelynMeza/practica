package com.example.ev;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ev.Api.Api;
import com.example.ev.Api.Servicios.ServicioPeticion;
import com.example.ev.ViewModels.MostrarRegistro;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity {
    private ListView Lista;
    private Button elboton, Otro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Lista=(ListView)findViewById(R.id.List);
        elboton = (Button)findViewById(R.id.button4);
        Otro = (Button) findViewById(R.id.button6);
        Otro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DetallesUsuarios.class);
                startActivity(intent);
            }
        });


       elboton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ServicioPeticion s = Api.getApi(MenuActivity.this).create(ServicioPeticion.class);
               Call<MostrarRegistro> R = s.getRegistro();
               R.enqueue(new Callback<MostrarRegistro>() {
                   @Override
                   public void onResponse(Call<MostrarRegistro> call, Response<MostrarRegistro> response) {
                       MostrarRegistro mos = response.body();
                        String Cad="";
                       ArrayList<String> ev = new ArrayList<String>();
                       if (mos.estado.equals("true")) {
                           for(Datos elemento : mos.usuarios
                           ){

                               Cad = Cad + elemento.id + " -- " + elemento.username + " -- " + elemento.password + " -- " + elemento.create_at + "\n";


                           }
                           ev.add( Cad);
                           ArrayAdapter adaptador = new ArrayAdapter(MenuActivity.this, android.R.layout.simple_list_item_1, ev);
                           Lista.setAdapter(adaptador);


                       }
                   }

                   @Override
                   public void onFailure(Call<MostrarRegistro> call, Throwable t) {

                   }
               });
           }
       });
    }}