package com.example.ev.Api.Servicios;

import com.example.ev.ViewModels.MostrarRegistro;
import com.example.ev.ViewModels.Peticion_Login;
import com.example.ev.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @POST("api/todosUsuarios")
    Call<MostrarRegistro> getRegistro();

    @FormUrlEncoded
    @POST("api/detallesUsuario")
    Call<MostrarRegistro> getUsuarioDetalle(@Field("usuarioId") int id);
}
