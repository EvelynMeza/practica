package com.example.ev;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.ev.Api.Api;
import com.example.ev.Api.Servicios.ServicioPeticion;
import com.example.ev.ViewModels.MostrarRegistro;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetallesUsuarios extends AppCompatActivity {
    private Button Reg, MD;
    private ListView ListaUsu;
    private EditText elu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_usuarios);
        Reg = (Button) findViewById(R.id.button8);
        MD= (Button) findViewById(R.id.button7);
        ListaUsu = (ListView)findViewById(R.id.ListU);
        elu = (EditText) findViewById(R.id.editText5);
        Reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });
        MD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicioPeticion s = Api.getApi(DetallesUsuarios.this).create(ServicioPeticion.class);
                int us = Integer.parseInt(elu.getText().toString());
                Call<MostrarRegistro> R = s.getUsuarioDetalle(us);
                R.enqueue(new Callback<MostrarRegistro>() {
                    @Override
                    public void onResponse(Call<MostrarRegistro> call, Response<MostrarRegistro> response) {
                        MostrarRegistro mos = response.body();
                        String Cad = "";
                        ArrayList<String> ev = new ArrayList<String>();
                        if (mos.estado.equals("true")) {
                            Cad= mos.usuario + "--" + mos.password;
                            ev.add(Cad);
                            ArrayAdapter adaptador = new ArrayAdapter(DetallesUsuarios.this, android.R.layout.simple_list_item_1, ev);
                            ListaUsu.setAdapter(adaptador);


                        }
                    }

                    @Override
                    public void onFailure(Call<MostrarRegistro> call, Throwable t) {

                    }

                    });
                    }
                });
            }
        }

