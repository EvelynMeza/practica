package com.example.ev.ViewModels;

import com.example.ev.Datos;

import java.util.List;

public class MostrarRegistro {
    public String estado;
    public List<Datos> usuarios;
    public String usuario;
    public String password;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Datos> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Datos> usuarios) {
        this.usuarios = usuarios;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
